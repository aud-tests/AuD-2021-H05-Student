### Version 2.1.0
Made generation of parentheses terms completely random (Thanks @jonas.renk!) \
Refactored checkForUpdates() in Utils.java


### Version 2.0.0
Changed format for metadata (Hashes, default configurations, etc.) to JSON \
Switched from major/minor version to Semantic Versioning \
Moved configurations to [`Config.java`](src/test/java/h05/Config.java) \
Made [`Assertions.java`](src/test/java/h05/Assertions.java) a subclass of JUnit's Assertions class


### Version 1.1
Fixed parentheses generation and tests (missing `\n` at the end) \
Fixed link to changelog


### Version 1.0
Initial tests \
Addendum: Optimized RandomTreeProvider