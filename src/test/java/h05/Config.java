package h05;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class contains constants that are used as settings for the tests.
 * They will not be overwritten when an update is downloaded (assuming this file is in {@link Config#EXCLUDED_FILES}),
 * but will be updated is such a way that everything between the lines containing ">>>##" and "##<<<" will be kept.
 * This includes any changes or appended code.
 */
@SuppressWarnings({"JavadocReference", "unused"})
public class Config {

    // >>>## UPDATE MARKER, DO NOT REMOVE, ONLY MODIFY THE LINES BELOW

    /**
     * Whether this file exists. Used by {@link Utils} to determine whether or not the config file is missing
     */
    public static final boolean EXISTS = true;

    /**
     * Seed that is used for initialization of {@link Utils#RANDOM}, set to fixed value for (hopefully) reproducible results
     */
    public static final long SEED = new Random().nextLong();

    /**
     * Settings for the updater / installer <br>
     * Set the values of these constants to {@code true} or {@code false} respectively, if you want or don't want to...
     * <ul>
     *     <li>{@code CHECK_FOR_UPDATES} - use the functionality at all</li>
     *     <li>{@code CHECK_HASHES} - compare the hashes of local files with the ones in the repository</li>
     *     <li>{@code AUTO_UPDATE} - let the updater / installer download files from the repository and overwrite the local files automatically</li>
     * </ul>
     * @see Utils.Updater
     */
    public static final boolean CHECK_FOR_UPDATES = true, CHECK_HASHES = true, AUTO_UPDATE = true;

    /**
     * A list of files (with path relative to project root) to be excluded from updates.
     * This does not prevent updates to this configuration file ({@link Config#AUTO_UPDATE} does that),
     * it just prevents complete overwrites
     * @see Config
     */
    public static final List<String> EXCLUDED_FILES = List.of(
            "src/test/java/h05/Config.java"
    );

    /**
     * Determines which build variant should be used in MyTree.
     * More specifically, what value to pass as the second parameter of the constructor.
     * If the value is {@code null} then the constructor will be called randomly with either {@code true} or {@code false}
     */
    public static final Boolean BUILD_VARIANT = null;

    /**
     * Allows customization of the number of test runs for a parameterized test method.
     * To override the number of runs add an entry consisting of the fully qualified class name +
     * '#' + the method signature mapped to an integer value (example below).
     * If the method is not in this map, a default value of 5 is used
     */
    public static final Map<String, Integer> NUMBER_OF_TEST_RUNS = Map.of(
            "h05.MyTreeTest#testIsIsomorphic(String)", 5
    );

    /**
     * Defines the maximum depth of a randomly generated parentheses term
     * @see h05.provider.RandomTreeProvider
     */
    public static final int MAX_TREE_DEPTH = 5;

    // ##<<< UPDATE MARKER, DO NOT REMOVE, DO NOT CHANGE ANYTHING BELOW THIS LINE

    /**
     * Returns a set of the names of all fields in this class
     * @return a set of all fields defined in this class
     */
    public static Set<String> getConfigs() {
        return Arrays.stream(Config.class.getDeclaredFields()).map(Field::getName).collect(Collectors.toUnmodifiableSet());
    }


}
