package h05;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Map;

import static h05.Assertions.*;
import static h05.Utils.*;
import static h05.Utils.TestType.Type.CLASS;
import static java.lang.reflect.Modifier.*;

@TestType(CLASS)
public class ListItemTest {

    public static Class<?> listItemClass;
    public static Constructor<?> constructor;
    public static Field key, next;

    private static final String CLASS_NAME = "h05.ListItem";

    @BeforeAll
    public static void checkClass() {
        if (CLASS_CORRECT.containsKey(ListItemTest.class))
            return;



        listItemClass = getClassForName(CLASS_NAME);

        // is generic
        assertIsGeneric(listItemClass, Map.of("T", UNBOUNDED));

        // is not abstract
        assertDoesNotHaveModifiers(listItemClass, ABSTRACT);

        // constructors
        try {
            constructor = listItemClass.getDeclaredConstructor();
        } catch (NoSuchMethodException e) {
            fail(CLASS_NAME + " must have a parameterless constructor", e);
        }

        // fields
        try {
            key = listItemClass.getDeclaredField("key");
            next = listItemClass.getDeclaredField("next");
        } catch (NoSuchFieldException e) {
            fail(CLASS_NAME + " is missing one or more required fields", e);
        }

        key.setAccessible(true);
        next.setAccessible(true);
        
        assertHasModifiers(key, PUBLIC);
        assertType(key, "T");
        
        assertHasModifiers(next, PUBLIC);
        assertType(next, "h05.ListItem<T>");



        CLASS_CORRECT.put(ListItemTest.class, true);
    }

    @Test
    public void classDefinitionCorrect() {}
}
