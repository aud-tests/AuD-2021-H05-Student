package h05;

import org.json.JSONObject;
import org.opentest4j.TestAbortedException;

import java.io.*;
import java.lang.annotation.*;
import java.lang.reflect.*;
import java.math.BigInteger;
import java.net.URI;
import java.net.http.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.*;

@SuppressWarnings("unused")
public class Utils {

    // ----------------------------------- //
    // DO NOT CHANGE ANYTHING IN THIS FILE //
    // ----------------------------------- //

    private static final long SEED = (Long) getConfigOrDefault("SEED", new Random().nextLong());
    private static final boolean CHECK_FOR_UPDATES = (Boolean) getConfigOrDefault("CHECK_FOR_UPDATES", true),
                                 CHECK_HASHES = (Boolean) getConfigOrDefault("CHECK_HASHES", true),
                                 AUTO_UPDATE = (Boolean) getConfigOrDefault("AUTO_UPDATE", true);

    public static final Random RANDOM = new Random(SEED);
    public static final Map<Class<?>, Boolean> CLASS_CORRECT = new HashMap<>();
    public static final Map<Method, Boolean> METHOD_CORRECT = new HashMap<>();

    private static final String LOCAL_VERSION = "2.1.0", ASSIGNMENT_ID = "H05";
    private static final Map<TestType.Type, String> METHOD_LOOKUP = Map.of(
            TestType.Type.CLASS, "checkClass",
            TestType.Type.INTERFACE, "checkInterface"
    );

    static {
        System.out.println("Reminder: remove these tests and the dependency from build.gradle.kts before submitting!");

        if (!(Boolean) getConfigOrDefault("EXISTS", false))
            System.err.println("Configurations could not be found, they must be downloaded from the repository. " +
                    "They will be automatically downloaded when using the installer or when CHECK_FOR_UPDATES and AUTO_UPDATE are true. " +
                    "Please re-run the tests afterwards.");

        if (!CHECK_FOR_UPDATES || !Updater.checkForUpdates()) {
            System.out.println("Seed: " + SEED);
        } else {
            System.out.println("Updated tests, please re-run");

            System.exit(0);
        }
    }

    /**
     * main method stub, test installer
     */
    public static void main(String[] args) {}

    /**
     * Returns the class object for a given name or throws an {@link TestAbortedException} if it is not found
     * @param className the fully qualified name of the class
     * @return the class object for the corresponding name
     */
    public static Class<?> getClassForName(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new TestAbortedException("Class " + e.getMessage() + " not found", e);
        }
    }

    /**
     * Creates a dependency on another class
     * <br><br>
     * Requires that all of the supplied class are annotated with {@link TestType}. <br>
     * Invokes the method specified in {@link Utils#METHOD_LOOKUP} and puts {@code true} in
     * the lookup table, or {@code false} if any exception was thrown. If a class exists in
     * the {@link Utils#CLASS_CORRECT} lookup table then that value is used without
     * invoking the method. <br>
     * If an invocation failed or the value for a class is {@code false}, a
     * {@link TestAbortedException} is thrown
     * @param classes the classes that are required by the calling test class
     */
    public static void requireTest(Class<?>... classes) {
        for (Class<?> c : classes) {
            String testClassName = c.getName(), actualClassName = testClassName.substring(0, testClassName.length() - 4);
            Exception e = null;

            if (CLASS_CORRECT.containsKey(c)) {
                if (CLASS_CORRECT.get(c))
                    continue;
            } else {
                try {
                    Method method = c.getDeclaredMethod(METHOD_LOOKUP.get(c.getDeclaredAnnotation(TestType.class).value()));

                    method.setAccessible(true);
                    method.invoke(null);

                    CLASS_CORRECT.put(c, true);
                    continue;
                } catch (Exception ee) {
                    CLASS_CORRECT.put(c, false);
                    e = ee;
                }
            }

            throw new TestAbortedException(
                    Thread.currentThread().getStackTrace()[2].getClassName() + " requires " + actualClassName + " to be implemented correctly", e);
        }
    }

    /**
     * Creates a dependency on another method
     * <br><br>
     * Requires that the declaring class is annotated with {@link TestType}. <br>
     * Invokes the given method and puts {@code true} in the lookup table, or {@code false}
     * if any exception was thrown. If the method exists in the
     * {@link Utils#METHOD_CORRECT} lookup table then that value is used without invoking
     * the method. <br>
     * If the invocation failed or the value for a class is {@code false}, a
     * {@link TestAbortedException} is thrown
     * @param method    the method to be invoked
     * @param args      the arguments to supply on invocation
     */
    public static void requireTest(Method method, Object... args) {
        if (METHOD_CORRECT.containsKey(method)) {
            if (METHOD_CORRECT.get(method))
                return;
            else
                throw new TestAbortedException(Thread.currentThread().getStackTrace()[2].getMethodName() + " requires that " +
                        method.getName() + " executes without any exceptions");
        }

        try {
            requireTest(method.getDeclaringClass());
        } catch (TestAbortedException e) {
            METHOD_CORRECT.put(method, false);
            throw e;
        }

        try {
            method.invoke(method.getDeclaringClass().getDeclaredConstructor().newInstance(), args);
            METHOD_CORRECT.put(method, true);
        } catch (ReflectiveOperationException e) {
            METHOD_CORRECT.put(method, false);

            throw new TestAbortedException(Thread.currentThread().getStackTrace()[2].getMethodName() + " requires that " +
                    method.getName() + " executes without any exceptions", e.getCause());
        }
    }

    /**
     * Annotation interface for determining what method should be invoked by
     * {@link Utils#requireTest(Class[])}
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    public @interface TestType {
        enum Type {CLASS, INTERFACE}

        Type value();
    }

    /**
     * Tries to invoke the given method with the given parameters and throws the actual
     * Throwable that caused the InvocationTargetException
     * @param method   the method to invoke
     * @param instance the instance to invoke the method on
     * @param params   the parameter to invoke the method with
     * @throws Throwable the actual Throwable (Exception)
     */
    public static void getActualException(Method method, Object instance, Object... params) throws Throwable {
        try {
            method.invoke(instance, params);
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        }
    }

    /**
     * Attempts to get the value of a given field from {@link Config}. Returns either the actual value or the given default value
     * @param fieldName    name of the field
     * @param defaultValue the default value to return should a exception occur
     * @return the actual value of the field or the given default value
     */
    public static Object getConfigOrDefault(String fieldName, Object defaultValue) {
        try {
            Class<?> c = Class.forName("h05.Config");

            return c.getDeclaredField(fieldName).get(null);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    private static class Updater {

        private static final String REPOSITORY_URL = "https://git.rwth-aachen.de/aud-tests/AuD-2021-" + ASSIGNMENT_ID + "-Student/-/";

        /**
         * Checks if the repository is newer than the local copy and updates it.
         * Messages are printed to let the user know what is happening
         * @return whether any changes have been written to disk
         * @see Config#CHECK_FOR_UPDATES
         */
        private static boolean checkForUpdates() {
            HttpResponse<String> response = getHttpResource(".test_metadata.json");
            boolean persistentChanges = false;

            if (response == null || response.statusCode() != 200) {
                System.err.println("Unable to fetch version from repository");
                return false;
            }

            JSONObject remoteData = new JSONObject(response.body());
            Version localVersion = new Version(LOCAL_VERSION),
                    remoteVersion = new Version(remoteData.getString("version"));

            if (remoteVersion.compareTo(localVersion) <= 0) {
                System.out.println("Tests are up to date");
                return false;
            }

            System.out.println("Update available! Local version: " + localVersion + " -- Remote version: " + remoteVersion);
            System.out.println("Changelog: " + REPOSITORY_URL + "blob/master/changelog.md");
            System.out.println(remoteData.getString("updateMessage"));

            if (!(CHECK_HASHES || AUTO_UPDATE))
                return false;

            try {
                for (Map.Entry<String, Object> fileMap : remoteData.getJSONObject("hashes").toMap().entrySet()) {
                    String fileName = fileMap.getKey(), expectedHash = (String) fileMap.getValue();

                    if (((List<?>) getConfigOrDefault("EXCLUDED_FILES", List.of())).contains(fileName))
                        continue;

                    if (!new File(fileName).exists()) {
                        if (AUTO_UPDATE)
                            persistentChanges = updateLocal(fileName);
                        else
                            System.err.println(fileName + " not found, can't compare hashes");

                        continue;
                    }

                    if (!getHash(fileName).equals(expectedHash)) {
                        System.out.println("Hash mismatch for file " + fileName);

                        if (AUTO_UPDATE)
                            persistentChanges = updateLocal(fileName);
                    }
                }

                JSONObject configData = remoteData.getJSONObject("config");
                Set<String> constants = getConstants();

                if (AUTO_UPDATE &&
                        ((Boolean) getConfigOrDefault("EXISTS", false)) &&
                        !constants.containsAll(configData.getJSONObject("constants").keySet())) {
                    updateConfig(configData);

                    persistentChanges = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return persistentChanges;
        }

        /**
         * Requests a resource / file from the repository
         * @param resource the resource to get from the repository
         * @return a {@link HttpResponse<String>} object
         */
        private static HttpResponse<String> getHttpResource(String resource) {
            HttpClient client = HttpClient.newBuilder()
                                          .version(HttpClient.Version.HTTP_2)
                                          .followRedirects(HttpClient.Redirect.NORMAL)
                                          .connectTimeout(Duration.ofSeconds(20))
                                          .build();
            HttpRequest request = HttpRequest.newBuilder(
                    URI.create(REPOSITORY_URL + "raw/master/" + resource)).build();

            try {
                return client.send(request, HttpResponse.BodyHandlers.ofString());
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * Calculates the MD5 hash of a file and returns it
         * @param fileName path to the file
         * @return the hash as a hexadecimal string
         * @throws IOException if an I/O error occurs
         */
        private static String getHash(String fileName) throws IOException {
            try {
                MessageDigest messageDigest = MessageDigest.getInstance("MD5");

                try (InputStream inputStream = new FileInputStream(fileName)) {
                    String actualHash = new BigInteger(1, messageDigest.digest(inputStream.readAllBytes())).toString(16);

                    return "0".repeat(32 - actualHash.length()) + actualHash;
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return "";
            }
        }

        /**
         * Updates (overwrites) the specified file with the contents of the file at the repository
         * @param fileName the relative path to the file
         * @return whether anything has been written to disk
         */
        private static boolean updateLocal(String fileName) {
            System.out.print("Downloading " + fileName + "... ");

            File file = new File(fileName);
            HttpResponse<String> response = getHttpResource(fileName);

            //noinspection ResultOfMethodCallIgnored
            file.getParentFile().mkdirs();

            if (response != null && response.statusCode() == 200) {
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                    writer.write(response.body());
                } catch (IOException e) {
                    e.printStackTrace();
                    return true;
                }

                System.out.println("done");

                return true;
            }

            System.out.println("unable to fetch file from repository");

            return false;
        }

        /**
         * Updates the configuration file, keeping all data written between the markers ">>>##" and "##<<<"
         * @param configData a JSON object containing information such as required constants, for example
         * @throws IOException if an I/O error occurs
         * @see Config
         */
        private static void updateConfig(JSONObject configData) throws IOException {
            String configStub = configData.getString("stub");
            StringBuilder configFileContents = new StringBuilder("    // >>>## UPDATE MARKER, DO NOT REMOVE, ONLY MODIFY THE LINES BELOW\n");
            Set<String> existingFields = getConstants();

            try (BufferedReader reader = new BufferedReader(new FileReader(configData.getString("file")))) {
                boolean insert = false;

                for (String line = reader.readLine(); !line.matches("\\s*// ##<<<.*"); line = reader.readLine()) {
                    if (line.matches("\\s*// >>>##.*")) {
                        line = reader.readLine();
                        insert = true;
                    }

                    if (insert)
                        configFileContents.append(line).append("\n");
                }
            }

            for (Map.Entry<String, Object> constantEntry : configData.getJSONObject("constants").toMap().entrySet()) {
                String fieldName = constantEntry.getKey(), fieldData = (String) constantEntry.getValue();

                if (!existingFields.contains(fieldName))
                    configFileContents.append(fieldData).append("\n\n");
            }

            configFileContents.append("    // ##<<< UPDATE MARKER, DO NOT REMOVE, DO NOT CHANGE ANYTHING BELOW THIS LINE\n\n");

            for (Map.Entry<String, Object> methodEntry : configData.getJSONObject("methods").toMap().entrySet())
                configFileContents.append((String) methodEntry.getValue()).append("\n\n");

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(configData.getString("file")))) {
                writer.write(configStub.replaceFirst(">>>##<<<", configFileContents.toString()));
            }
        }

        /**
         * Gets a set of all constants defined in {@link Config} (invokes {@link Config#getConfigs()})
         * @return the set of constants or an empty set if a {@link ReflectiveOperationException} was thrown
         */
        private static Set<String> getConstants() {
            try {
                //noinspection unchecked
                return (Set<String>) Class.forName("h05.Config").getDeclaredMethod("getConfigs").invoke(null);
            } catch (ReflectiveOperationException e) {
                return Set.of();
            }
        }

        private static class Version implements Comparable<Version> {

            private final int MAJOR_VERSION, MINOR_VERSION, PATCH_VERSION;

            private Version(String version) {
                String[] versions = version.split("\\.");

                MAJOR_VERSION = Integer.parseInt(versions[0]);
                MINOR_VERSION = Integer.parseInt(versions[1]);
                PATCH_VERSION = Integer.parseInt(versions[2]);
            }

            @Override
            public int compareTo(Version version) {
                int versionDiffMajor = MAJOR_VERSION - version.MAJOR_VERSION,
                    versionDiffMinor = MINOR_VERSION - version.MINOR_VERSION,
                    versionDiffPatch = PATCH_VERSION - version.PATCH_VERSION;

                if (versionDiffMajor != 0)
                    return versionDiffMajor;
                else if (versionDiffMinor != 0)
                    return versionDiffMinor;
                else
                    return versionDiffPatch;
            }

            @Override
            public String toString() {
                return MAJOR_VERSION + "." + MINOR_VERSION + "." + PATCH_VERSION;
            }
        }
    }
}
