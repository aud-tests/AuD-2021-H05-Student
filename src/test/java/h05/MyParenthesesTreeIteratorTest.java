package h05;

import h05.provider.RandomTreeProvider;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static h05.Assertions.*;
import static h05.Utils.TestType.Type.CLASS;
import static h05.Utils.*;
import static java.lang.reflect.Modifier.*;

@TestType(CLASS)
public class MyParenthesesTreeIteratorTest {

    public static Class<?> myParenthesesTreeIteratorClass;

    private static final String CLASS_NAME = "h05.MyParenthesesTreeIterator";

    @BeforeAll
    public static void checkClass() {
        if (CLASS_CORRECT.containsKey(MyParenthesesTreeIteratorTest.class))
            return;

        requireTest(MyTreeTest.class);



        myParenthesesTreeIteratorClass = getClassForName(CLASS_NAME);

        // modifiers
        assertDoesNotHaveModifiers(myParenthesesTreeIteratorClass, ABSTRACT);

        // is not generic
        assertNotGeneric(myParenthesesTreeIteratorClass);

        // implements Iterator<Character>
        assertImplements(myParenthesesTreeIteratorClass, "java.util.Iterator<java.lang.Character>");



        CLASS_CORRECT.put(MyParenthesesTreeIteratorTest.class, true);
    }

    @ParameterizedTest
    @ArgumentsSource(RandomTreeProvider.class)
    public void testNext(String treeString) throws ReflectiveOperationException {
        requireTest(MyTreeTest.class.getDeclaredMethod("testIterator"));

        treeString = treeString.substring(0, treeString.indexOf('\n') + 1);
        Iterator<Character> iterator = MyTreeTest.getIterator(treeString);
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < treeString.length() - 1; i++)
            try {
                Character next = iterator.next();

                builder.append(next != null ? next : "");
            } catch (NoSuchElementException e) {
                fail("Iterator has fewer elements than expected: " + (i + 1) + "/" + (treeString.length() - 1), e);
            }

        assertEquals(treeString.substring(0, treeString.length() - 1), builder.toString(),
                "Characters returned by iterator does not equal expected");
        assertThrows(NoSuchElementException.class, iterator::next, "Iterator has more elements than expected");
    }

    @ParameterizedTest
    @ArgumentsSource(RandomTreeProvider.class)
    public void testHasNext(String treeString) throws ReflectiveOperationException {
        requireTest(MyTreeTest.class.getDeclaredMethod("testIterator"));
        requireTest(MyParenthesesTreeIteratorTest.class.getDeclaredMethod("testNext", String.class), "()\n");

        treeString = treeString.substring(0, treeString.indexOf('\n') + 1);
        Iterator<Character> iterator = MyTreeTest.getIterator(treeString);

        for (int i = 0; i < treeString.length() - 1; i++, iterator.next())
            assertTrue(iterator.hasNext(),
                    "Iterator should not have reached end yet, number of elements: " + (i + 1) + "/" + (treeString.length() - 1));

        assertFalse(iterator.hasNext(), "Iterator has more elements than expected");
    }

    @Test
    public void testRemove() throws ReflectiveOperationException {
        Iterator<Character> iterator = MyTreeTest.getIterator("\n");

        assertThrows(UnsupportedOperationException.class, iterator::remove);
    }
}
