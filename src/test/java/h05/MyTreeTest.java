package h05;

import h05.provider.RandomTreeProvider;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import javax.management.BadStringOperationException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Stack;

import static h05.Assertions.*;
import static h05.Config.*;
import static h05.Utils.*;
import static h05.Utils.TestType.Type.CLASS;
import static h05.provider.RandomTreeProvider.treeToString;
import static java.lang.reflect.Modifier.*;

@SuppressWarnings("ConstantConditions")
@TestType(CLASS)
public class MyTreeTest {

    public static Class<?> myTreeClass;
    public static Constructor<?> constructor;
    public static Field root, nextNodeID;
    public static Method buildRecursively, buildIteratively, iterator, isIsomorphic;

    private static final String CLASS_NAME = "h05.MyTree";
    private static boolean printedWarning = false;

    @BeforeAll
    public static void checkClass() {
        if (CLASS_CORRECT.containsKey(MyTreeTest.class))
            return;

        requireTest(MyTreeNodeTest.class);



        myTreeClass = getClassForName(CLASS_NAME);

        // modifiers
        assertDoesNotHaveModifiers(myTreeClass, ABSTRACT);

        // is not generic
        assertNotGeneric(myTreeClass);

        // implements Iterable<Character>
        assertImplements(myTreeClass, "java.lang.Iterable<java.lang.Character>");

        // constructors
        try {
            constructor = myTreeClass.getDeclaredConstructor(Reader.class, boolean.class);
        } catch (NoSuchMethodException e) {
            fail(CLASS_NAME + " is missing a required constructor", e);
        }

        assertHasModifiers(constructor, "Constructor of " + CLASS_NAME + " must be public", PUBLIC);

        // fields
        try {
            root = myTreeClass.getDeclaredField("root");
            nextNodeID = myTreeClass.getDeclaredField("nextNodeID");
        } catch (NoSuchFieldException e) {
            fail(CLASS_NAME + " is missing one or more required fields", e);
        }

        root.setAccessible(true);
        nextNodeID.setAccessible(true);

        assertHasModifiers(root, PRIVATE);
        assertType(root, MyTreeNodeTest.myTreeNodeClass.getTypeName());

        assertHasModifiers(nextNodeID, PRIVATE, STATIC);
        assertType(nextNodeID, long.class.getTypeName());

        // methods
        try {
            buildRecursively = myTreeClass.getDeclaredMethod("buildRecursively", Reader.class);
            buildIteratively = myTreeClass.getDeclaredMethod("buildIteratively", Reader.class, Stack.class);
            iterator = myTreeClass.getDeclaredMethod("iterator");
            isIsomorphic = myTreeClass.getDeclaredMethod("isIsomorphic", myTreeClass);
        } catch (NoSuchMethodException e) {
            fail(CLASS_NAME + " is missing one or more required methods", e);
        }

        assertHasModifiers(buildRecursively, PRIVATE);
        assertReturnType(buildRecursively, MyTreeNodeTest.myTreeNodeClass.getTypeName());
        assertTrue(() -> {
            boolean hasBadStringOperationException = false, hasIOException = false;

            for (Class<?> exception : buildRecursively.getExceptionTypes())
                if (exception.equals(BadStringOperationException.class))
                    hasBadStringOperationException = true;
                else if (exception.equals(IOException.class))
                    hasIOException = true;

            return hasBadStringOperationException && hasIOException;
        }, "buildRecursively does not throw all required potential exceptions");

        assertHasModifiers(buildIteratively, PRIVATE);
        assertReturnType(buildIteratively, MyTreeNodeTest.myTreeNodeClass.getTypeName());
        assertTrue(() -> {
            boolean hasBadStringOperationException = false, hasIOException = false;

            for (Class<?> exception : buildIteratively.getExceptionTypes())
                if (exception.equals(BadStringOperationException.class))
                    hasBadStringOperationException = true;
                else if (exception.equals(IOException.class))
                    hasIOException = true;

            return hasBadStringOperationException && hasIOException;
        }, "buildIteratively does not throw all required potential exceptions");

        assertHasModifiers(isIsomorphic, PUBLIC);
        assertReturnType(isIsomorphic, boolean.class.getTypeName());



        CLASS_CORRECT.put(MyTreeTest.class, true);
    }

    @ParameterizedTest
    @ArgumentsSource(RandomTreeProvider.class)
    void testConstructorWithValid(String treeString) throws ReflectiveOperationException, IOException {
        boolean recursive = BUILD_VARIANT == null ? RANDOM.nextBoolean() : BUILD_VARIANT;
        StringReader reader = new StringReader(treeString);
        int endOfTree = treeString.indexOf('\n');

        assertEquals(treeString.substring(0, endOfTree), treeToString(root.get(getNewInstance(reader, recursive)), true),
                "Trees do not match, called constructor with second parameter = " + recursive);
        assertRestOfReaderMatches(treeString.substring(endOfTree + 1), reader);
    }

    @ParameterizedTest
    @ArgumentsSource(RandomTreeProvider.class)
    void testConstructorWithInvalid(String treeString) {
        boolean recursive = BUILD_VARIANT == null ? RANDOM.nextBoolean() : BUILD_VARIANT;

        assertThrows(BadStringOperationException.class, () -> {
            try {
                String invalidTreeString = treeString.replaceFirst(
                        "\n", RANDOM.nextBoolean() ? "" : RANDOM.nextBoolean() ? "(\n" : ")\n");

                getNewInstance(invalidTreeString, recursive);
            } catch (ReflectiveOperationException e) {
                throw e.getCause();
            }
        }, "Constructor did not throw BadStringOperationException, called with second parameter = " + recursive);
    }

    private void assertRestOfReaderMatches(String rest, StringReader reader) throws IOException {
        for (int i = 0; i < rest.length(); i++)
            assertEquals(rest.charAt(i), reader.read(), "Reader did not return correct character");

        assertEquals(-1, reader.read(), "Reader should not have any characters left");
    }

    @Test
    public void testIterator() throws ReflectiveOperationException {
        requireTest(MyParenthesesTreeIteratorTest.class);

        assertEquals(MyParenthesesTreeIteratorTest.myParenthesesTreeIteratorClass, iterator.invoke(getNewInstance("\n")).getClass(),
                "Returned object does not have type MyParenthesesTreeIterator");
    }

    @SuppressWarnings("unchecked")
    public static Iterator<Character> getIterator(String treeString) throws ReflectiveOperationException {
        return (Iterator<Character>) iterator.invoke(getNewInstance(treeString));
    }

    @ParameterizedTest
    @ArgumentsSource(RandomTreeProvider.class)
    public void testIsIsomorphic(String treeString) throws ReflectiveOperationException {
        Object instance1 = getNewInstance(treeString),
               instance2 = getNewInstance(treeString);

        assertTrue((Boolean) isIsomorphic.invoke(instance1, instance2), "Trees are isomorphic");
        assertTrue((Boolean) isIsomorphic.invoke(instance2, instance1), "Trees are isomorphic");
        assertTrue((Boolean) isIsomorphic.invoke(instance1, instance1), "Trees are isomorphic");
        assertTrue((Boolean) isIsomorphic.invoke(instance2, instance2), "Trees are isomorphic");

        String nonIsomorphicTreeString = "(".repeat(MAX_TREE_DEPTH + 1) + ")".repeat(MAX_TREE_DEPTH + 1) + "\n";
        Object nonIsomorphicInstance = getNewInstance(nonIsomorphicTreeString);

        assertFalse((Boolean) isIsomorphic.invoke(instance1, nonIsomorphicInstance), "Trees are not isomorphic");
        assertFalse((Boolean) isIsomorphic.invoke(nonIsomorphicInstance, instance1), "Trees are not isomorphic");
        assertFalse((Boolean) isIsomorphic.invoke(instance2, nonIsomorphicInstance), "Trees are not isomorphic");
        assertFalse((Boolean) isIsomorphic.invoke(nonIsomorphicInstance, instance2), "Trees are not isomorphic");
    }

    private static Object getNewInstance(String treeString) throws ReflectiveOperationException {
        return getNewInstance(treeString, BUILD_VARIANT == null ? RANDOM.nextBoolean() : BUILD_VARIANT);
    }

    private static Object getNewInstance(String treeString, boolean recursive) throws ReflectiveOperationException {
        return getNewInstance(new StringReader(treeString), recursive);
    }

    private static Object getNewInstance(Reader reader, boolean recursive) throws ReflectiveOperationException {
        if (BUILD_VARIANT != null && !printedWarning) {
            System.out.println("BUILD_VARIANT is " + BUILD_VARIANT + ". Will only test " +
                    (BUILD_VARIANT ? "buildRecursively" : "buildIteratively"));

            printedWarning = true;
        }

        return constructor.newInstance(reader, recursive);
    }
}
