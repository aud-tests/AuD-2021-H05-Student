package h05.provider;

import h05.ListItemTest;
import h05.MyTreeNodeTest;
import h05.Utils;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.StringJoiner;
import java.util.stream.Stream;

import static h05.Config.*;

public class RandomTreeProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
        return Stream.generate(RandomTreeProvider::randomTreeArguments)
                     .limit(NUMBER_OF_TEST_RUNS.getOrDefault(
                             context.getRequiredTestMethod().getDeclaringClass().getName() + "#" + context.getDisplayName(),
                             5));
    }

    private static Arguments randomTreeArguments() {
        String treeString = randomTreeString(MAX_TREE_DEPTH);

        return Arguments.of(treeString + '\n' + (char) ('A' + Utils.RANDOM.nextInt(26)));
    }

    private static String randomTreeString(int maxTreeDepth) {
        double x = Utils.RANDOM.nextDouble();

        if (x < 0.1 || maxTreeDepth == 0)
            return "";
        else if (x < 0.4)
            return "(" + randomTreeString(maxTreeDepth - 1) + ")";

        return randomTreeString(maxTreeDepth - 1) + randomTreeString(maxTreeDepth - 1);
    }

    public static String treeToString(Object root, boolean isRoot) throws ReflectiveOperationException {
        StringJoiner joiner = new StringJoiner("");

        for (Object p = MyTreeNodeTest.successors.get(root); p != null; p = ListItemTest.next.get(p))
            joiner.add(treeToString(ListItemTest.key.get(p), false));

        return isRoot ? joiner.toString() : "(" + joiner + ")";
    }
}
