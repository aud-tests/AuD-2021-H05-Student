package h05;

import h05.Utils.TestType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import static h05.Assertions.*;
import static h05.Utils.*;
import static h05.Utils.TestType.Type.CLASS;
import static java.lang.reflect.Modifier.*;

@TestType(CLASS)
public class MyTreeNodeTest {

    public static Class<?> myTreeNodeClass;
    public static Constructor<?> constructor;
    public static Field nodeID, successors;
    
    private static final String CLASS_NAME = "h05.MyTreeNode";

    @BeforeAll
    public static void checkClass() {
        if (CLASS_CORRECT.containsKey(MyTreeNodeTest.class))
            return;

        requireTest(ListItemTest.class);



        myTreeNodeClass = getClassForName(CLASS_NAME);

        // modifiers
        assertHasModifiers(myTreeNodeClass, PUBLIC);
        assertDoesNotHaveModifiers(myTreeNodeClass, ABSTRACT);

        // is not generic
        assertNotGeneric(myTreeNodeClass);

        // constructors
        try {
            constructor = myTreeNodeClass.getDeclaredConstructor(long.class);
        } catch (NoSuchMethodException e) {
            fail(CLASS_NAME + " is missing a required constructor", e);
        }

        assertHasModifiers(constructor, "Constructor of " + CLASS_NAME + " must be public", PUBLIC);

        // fields
        try {
            nodeID = myTreeNodeClass.getDeclaredField("nodeID");
            successors = myTreeNodeClass.getDeclaredField("successors");
        } catch (NoSuchFieldException e) {
            fail(CLASS_NAME + " is missing one or more required fields", e);
        }

        assertHasModifiers(nodeID, PUBLIC, FINAL);
        assertType(nodeID, long.class.getTypeName());

        assertHasModifiers(successors, PUBLIC);
        assertType(successors, "h05.ListItem<h05.MyTreeNode>");



        CLASS_CORRECT.put(MyTreeNodeTest.class, true);
    }

    @Test
    public void testFields() throws ReflectiveOperationException {
        Object instance = constructor.newInstance(0);

        assertEquals(0L, nodeID.get(instance), "Field nodeID has wrong value after instantiation");
        assertNull(successors.get(instance), "Field successors has wrong value after instantiation");
    }
}
