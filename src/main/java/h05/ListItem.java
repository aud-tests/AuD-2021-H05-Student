package h05;

public class ListItem<T> {
  /**
   * Public no-args constructor DO NOT DELETE
   */
  public ListItem() {

  }

  // DO NOT RENAME THESE
  public T key;
  public ListItem<T> next;
}
