# Community Tests für die fünfte Hausübung der AuD 2021

**Änderung ab Version 2.0.0**: Die Tests sind ab Version 2.0.0 möglicherweise nicht zu 100% rückwärtskompatibel und sollten deswegen erneut heruntergeladen werden. Außerdem hängen sie von der Bibliothek JSON-java ab. Mehr dazu [hier](https://git.rwth-aachen.de/groups/aud-tests/-/wikis/JSON-Bibliothek). \
Einstellungen für die Tests wurden in die Datei [`Config.java`](src/test/java/h05/Config.java) ausgelagert. Dort findet sich auch die Dokumentation über die jeweiligen Konstanten.

<br>

Zum Ausführen der Tests sollte eine [eigene JUnit Run Configuration](https://git.rwth-aachen.de/groups/aud-tests/-/wikis/JUnit-Run-Configuration) angelegt werden, da durch den gradle task nicht alle Meldungen angezeigt werden (z.B. warum Tests ignoriert werden).

Da einige Methoden intern nach bestimmten Anforderungen aufgebaut sein müssen, aber man mit Black-box testing (worunter diese Tests fallen) dabei nur bedingt Aussagen über die Korrektheit treffen kann, weil die Ergebnisse identisch sind, wird bei manchen Tests nur geprüft, ob die jeweiligen Definitionen und Rückgaben korrekt sind. Ob das bei einem bestimmten Test der Fall ist, steht in den Beschreibungen der jeweiligen Methoden.

Mit * markierte Methoden testen, ob die jeweilige Klasse bzw. Interface korrekt definiert ist. Sie sind an sich keine Testmethoden und nur in Verbindung mit "richtigen" Tests, also mit `@Test` oder vergleichbar annotierten Methoden, brauchbar.

Die Tests haben einen Update-Mechanismus / Installer, der vor jeder Ausführung nach Updates sucht. Da das einige Sekunden in Anspruch nehmen kann und vielleicht auch anderweitig nicht gewünscht ist, kann diese Funktionalität ausgeschaltet werden. Wird diese Funktionalität verwendet, muss das Arbeitsverzeichnis gleich dem Projektordner sein. Das Verhalten kann mit Änderung der folgenden Konstanten in [`Config.java`](src/test/java/h05/Config.java) verändert werden:
- `CHECK_FOR_UPDATES` <br>
  Bestimmt, ob nach Updates gesucht wird. Ist diese Konstante `true`, dann wird bei jeder Ausführung der Tests nach Updates in diesem Repository gesucht und eine Meldung ausgegeben, sollten welche verfügbar sein. Ist sie `false` wird nicht nach Updates gesucht und andere Einstellungen werden ignoriert.
- `CHECK_HASHES` <br>
  Gibt an, ob die MD5-Hashes der lokalen Tests mit denen im Repository abgeglichen werden sollen. Hat die Konstante den Wert `true`, dann werden die Hashes der Dateien in [`.test_metadata.json`](.test_metadata.json) mit den tatsächlichen verglichen und eine Meldung ausgegeben, sollten sie nicht übereinstimmen.
- `AUTO_UPDATE` <br>
  Entscheidet, ob verfügbare Updates automatisch heruntergeladen werden sollen. Wenn aktiviert, werden Dateien, deren Hashes nicht übereinstimmen, erneut aus dem Repository heruntergeladen, wenn ein Update verfügbar ist. Diese Option ignoriert `CHECK_HASHES`, sollte die lokale Kopie der Tests veraltet sein. Da der Inhalt der Dateien überschrieben wird, werden lokale Änderungen an den Tests verloren gehen (gilt standardmäßig nicht für `Config.java`)!

Standardmäßig sind alle Optionen aktiviert.

DISCLAIMER: Das Durchlaufen der Tests ist keine Garantie dafür, dass die Aufgaben vollständig korrekt implementiert sind.

<br>

## ListItemTest

### .checkClass()* / .classDefinitionCorrect()
Überprüft, ob die Definition der Klasse `ListItem` korrekt ist. Testet, dass …
- die Klasse generisch ist und einen Typparameter `T` hat
- die Klasse nicht abstrakt ist
- die Klasse einen parameterlosen Konstruktor hat
- die Klasse die Attribute `key` vom Typ `T`und `next` vom Typ `ListItem<T>` hat


## MyTreeNodeTest

Setzt voraus, dass `ListItem` richtig definiert ist.

### .checkClass()*
Überprüft, ob die Definition der Klasse `MyTreeNode` korrekt ist. Testet, dass …
- die Klasse public ist
- die Klasse nicht generisch ist
- die Klasse nicht abstrakt ist
- die Klasse einen public Konstruktor mit formalem Typ `long` als ersten Parameter hat
- die Klasse die im Übungsblatt vorausgesetzten Attribute `nodeID` und `successors` besitzt

### .testFields()

Testet, ob die Attribute eines Objekts der Klasse die korrekten Werte nach der Instantiierung haben.


## MyTreeTest

Setzt voraus, dass `MyTreeNode` richtig definiert ist.

### .checkClass()*
Überprüft, ob die Definition der Klasse `MyTree` korrekt ist. Testet, dass …
- die Klasse nicht generisch ist
- die Klasse das Interface `Iterable<Character>` implementiert und nicht abstrakt ist
- die Klasse einen public Konstruktor mit einem Parameter vom formalen Typ `Reader` und einen Parameter vom formalen Typen `boolean` hat
- die Klasse die im Übungsblatt vorausgesetzten Attribute `root` und `nextNodeID` besitzt

### .testConstructor(String, Boolean)
Überprüft, ob der Konstruktor die Variable `root` richtig initialisiert und testet dabei indirekt die Methoden `buildRecursively` und `buildIteratively`. Der Konstruktor wird mit vorgegebenen Strings (valide sowie invalide Klammerausdrücke) als ersten Parameter und zufällig mit `true` und `false` als zweiten Parameter aufgerufen.

### .testIterator()
Überprüft, ob die Methode `iterator` eine Instanz von `MyParenthesesTreeIterator` zurückgibt.

### .testIsIsomorphic(String)
Überprüft, ob die Methode `isIsomorphic` korrekt funktioniert.


## ParenthesesTreeIteratorTest

Setzt voraus, dass `MyTree` richtig definiert ist.

### .checkClass()*
Überprüft, ob die Definition der Klasse `MyParenthesesTreeIterator` korrekt ist. Testet, dass …
- die Klasse nicht generisch ist
- die Klasse das Interface `Iterator<Character>` implementiert und nicht abstrakt ist

### .testNext(String)
Setzt voraus, dass `MyTree#iterator()` richtig definiert ist.
Überprüft, ob die Methode `next` mit zufälligen Klammerausdrücken wie erwartet funktioniert.

### .testHasNext(String)
Setzt voraus, dass `MyTree#iterator()` und `MyParenthesesTreeIterator#next()` richtig definiert sind.
Überprüft, ob die Methode `hasNext` mit zufälligen Klammerausdrücken wie erwartet funktioniert.

### .testRemove()
Überprüft, ob die Methode `remove` wie erwartet nur eine `UnsupportedOperationException` wirft.
